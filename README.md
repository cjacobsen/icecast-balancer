https://gitlab.com/cjacobsen/icecast-balancer

# Icecast Balancer

Monitors Icecast instances in your cluster and redirects the new 
connections to the less loaded and nearest server.

# Requirements

* PHP
* PDO
* SQlite

# Instalation

* Copy the file `config.txt.example` to `config.txt` and edit 
  accordingly. This file defines the base URL of each node of the 
  cluster.

Icecast Balancer is released under license GPLv3
