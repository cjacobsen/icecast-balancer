<?php
/*
This file is part of Icecast Balancer.

Icecast Balancer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Icecast Balancer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
$main = 'audio'; // 'audio' | 'video'
$title = 'Marat&oacute;n Linuxero';
$video_width = 640;
$video_height = 360;
$chat_width = 640;
$chat_height = 360;
$poster = 'poster.png';

$url_base = 'https://emision.maratonlinuxero.org/'; //Para referencia absoluta
//$url_base = ''; //Para referencia relativa

$extension = ($main == 'video' ? '.webm' : '.ogg');
$balancer_main = $url_base.'redirect.php?m=emision'.$extension;
$balancer_audiohd_ogg = $url_base.'redirect.php?m=audio-hd.ogg';
$balancer_audiosd_ogg = $url_base.'redirect.php?m=audio-sd.ogg';
$balancer_audiohd_mp3 = $url_base.'redirect.php?m=audio-hd.mp3';
$balancer_audiosd_mp3 = $url_base.'redirect.php?m=audio-sd.mp3';

$irc_server = 'irc.freenode.net';
//$irc_server = 'irc.irc-hispano.org';
$irc_port = '6667';
$irc_channel = 'maratonlinuxero';

//======================================================================
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
header('Access-Control-Allow-Origin: *'); 
$timestamp = time();
?>
<!DOCTYPE html>
<html manifest="manifest.appcache">
<head>
	<title><?php echo $title ?></title>
	<meta http-equiv="expires" content="Sun, 01 Jan 2000 00:00:00 GMT"/>
	<meta http-equiv="pragma" content="no-cache" />
	<style>
		body {
			margin: 0;
			padding: 0;
		}
		video {
			border: 1px solid #ccc;
		}
		#content {
			width: 640px;
			float: left;
		}
		#buttons {
			width: 150px;
			float: left;
		}
		#chat {
			width: 550px;
			float: left;
		}

		.btn {
			width: 100%;
		}
		
	</style>
</head>
<body>
<div id="buttons">
<?php
switch($main) {
	case 'audio':
		echo "<input class=\"btn\" value=\"Audio HD OGG\" type=\"button\" onclick=\"reload_main();\">";
		break;
	case 'video':
		echo "<input class=\"btn\" value=\"Video\" type=\"button\" onclick=\"reload_main();\">";
		echo "<input class=\"btn\" value=\"Audio HD OGG\" type=\"button\" onclick=\"reload_audiohd_ogg();\">";
		break;
}
?>
	<input class="btn" value="Audio SD OGG" type="button" onclick="reload_audiosd_ogg();">
	<input class="btn" value="Audio HD MP3" type="button" onclick="reload_audiohd_mp3();">
	<input class="btn" value="Audio SD MP3" type="button" onclick="reload_audiosd_mp3();">
	<input class="btn" value="Reload Chat" type="button" onclick="reload_chat();">
</div>
<div id="content">
	<video id="media" width="<?php echo $video_width ?>" height="<?php echo $video_height ?>" controls autoplay="true" poster="<?php echo $poster ?>" src="<?php echo $balancer_main ?>&t=<?php echo $timestamp ?>">
	No HTML 5 support
	</video>
</div>
<div id="chat">
	<iframe id="irc" src="https://kiwiirc.com/client/<?php echo $irc_server.':'.$irc_port ?>/#<?php echo $irc_channel ?>" style="border:0; width: 100%; height:<?php echo $chat_height ?>px;"></iframe>
</div>
<script>
	function reload_main() {
		document.getElementById('media').src = '<?php echo $balancer_main ?>&t=' + Math.round((new Date()).getTime() / 1000);
	}

	function reload_audiohd_ogg() {
		document.getElementById('media').src = '<?php echo $balancer_audiohd_ogg ?>&t=' + Math.round((new Date()).getTime() / 1000);
	}

	function reload_audiosd_ogg() {
		document.getElementById('media').src = '<?php echo $balancer_audiosd_ogg ?>&t=' + Math.round((new Date()).getTime() / 1000);
	}

	function reload_audiohd_mp3() {
		document.getElementById('media').src = '<?php echo $balancer_audiohd_mp3 ?>&t=' + Math.round((new Date()).getTime() / 1000);
	}

	function reload_audiosd_mp3() {
		document.getElementById('media').src = '<?php echo $balancer_audiosd_mp3 ?>&t=' + Math.round((new Date()).getTime() / 1000);
	}

	function reload_chat() {
		document.getElementById('irc').src += '';
	}
</script>
</body>
</html>
