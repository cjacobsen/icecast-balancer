<?php
/*
This file is part of Icecast Balancer.

Icecast Balancer is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Icecast Balancer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
*/
$debug = 0;
$config_file = 'config.php';
if(!file_exists($config_file)) {
	die("<style>code { background: hsl(220, 80%, 90%); }</style><h1>Icecast Balancer not yet configured<br/>Copy <code>{$config_file}.example</code> to <code>{$config_file}</code></h1>");
}
include($config_file);
$status_timeout = 60; //in seconds
$dbpath = 'redirect.db';
//----------------------------------------------------------------------

if(array_key_exists('m', $_REQUEST) and preg_match('/^([a-zA-Z0-9\.\-_]+)$/', trim($_REQUEST['m']), $arr)) {
	$mount = $arr[1];
}
else {
	die("Mountpoint undefined");
}

//Init DB
$db = new PDO('sqlite:'.$dbpath); 
initDB($db);
//Update status
if(agedStatus()) {
	touchTimestamp();
	foreach($nodes as $node) {
		$status = getStatus($node);
		saveStatus($status, $node);
	}
}

$url = selectNode($mount);
if($url) {
	debug("Redirecting to $url...");
	header("X-Powered-By: Icecast Balancer https://gitlab.com/cjacobsen/icecast-balancer");
	header('Location: '.$url);
}
else {
	debug("Aqui no hay salida!!!");
}
////////////////////////////////////////////////////////////////////////


function getStatus($node) {
	$url = $node."/status-json.xsl";
	$raw = file_get_contents($url);
	$status = json_decode($raw);
	if($status) {
		$ret = $status;
	}
	else {
		$ret = null;
	}
	//print_r($status);
	return $ret;
}

function saveStatus($status, $node) {
	global $db;
	if(!$status) {
		debug("Error: No status");
	}
	elseif(!isset($status->icestats->source)) { //No source
		debug("No source");
		$db->exec("DELETE FROM sources WHERE host = '{$status->icestats->host}'");
		debug($db->errorInfo());
	}
	elseif(is_array($status->icestats->source)) { //Many sources
		debug("Many sources");
		foreach($status->icestats->source as $source) {
			preg_match('/.*(\/[a-zA-Z0-9\.\-_]+)$/', $source->listenurl, $arr);
			$db->exec("REPLACE INTO sources VALUES ('{$status->icestats->host}', '{$node}{$arr[1]}', '{$source->listeners}', '".time()."', 0)");
			debug($db->errorInfo());
		}
	}
	elseif(is_object($status->icestats->source)) { //One source
		debug("One source");
		preg_match('/.*(\/[a-zA-Z0-9\.\-_]+)$/', $status->icestats->source->listenurl, $arr);
		$db->exec("REPLACE INTO sources VALUES ('{$status->icestats->host}', '{$node}{$arr[1]}', '{$status->icestats->source->listeners}', '".time()."', 0)");		
		debug($db->errorInfo());
	}
	else { //No idea
		debug("Error: Unknown");
	}
}

function agedStatus() {
	global $db, $status_timeout;
	$query = $db->query('SELECT value FROM vars WHERE name = "last_run"');
	$last_run = $query->fetchColumn();
	$age = time() - $last_run;
	return ($age > $status_timeout);
}

function touchTimestamp() {
	global $db;
	$query = $db->query('UPDATE vars SET value = "'.time().'" WHERE name = "last_run"');
}

function initDB($db) {
	if(!$db->query('SELECT * FROM sources')) {
			$db->query('CREATE TABLE sources (host TEXT, mount TEXT, listeners INT, timestamp INT, redirected INT, PRIMARY KEY(host, mount))');
			$db->query('CREATE TABLE vars (name TEXT, value INT)');
			$db->query('INSERT INTO vars VALUES ("last_run", "0")');
	}
}

function selectNode($mount) {
	global $db;
	//Get less used source
	$query = $db->prepare('SELECT mount FROM sources WHERE mount LIKE ? ORDER BY (listeners + redirected) ASC');
	$query->execute(array('%'.$mount));
	$url = $query->fetch();
	//Increment redirect counter
	$query = $db->prepare('UPDATE sources SET redirected = redirected + 1 WHERE mount = ?');
	$query->execute(array($url['mount']));
	return $url['mount'];
}

function debug($msg) {
	global $debug;
	if($debug) {
		echo "<pre>/n";
		print_r($msg);
		echo "\n";
		echo "</pre>/n";
	}
}